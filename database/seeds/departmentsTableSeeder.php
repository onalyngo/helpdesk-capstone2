<?php

use Illuminate\Database\Seeder;

class departmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        0=>array(
            'id'=>1,
            'name'=>'Administration'
            'created_at'=>null,
            'updated_at'=>null
        ),
        1=>array(
            'id'=>2,
            'name'=>'Human Resource'
            'created_at'=>null,
            'updated_at'=>null
        ),
        2=>array(
            'id'=>3,
            'name'=>'Information Technology'
            'created_at'=>null,
            'updated_at'=>null
        ),
        3=>array(
            'id'=>4,
            'name'=>'Accounting: Payroll'
            'created_at'=>null,
            'updated_at'=>null
        )
    }
}
