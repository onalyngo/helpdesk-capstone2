<?php

use Illuminate\Database\Seeder;

class itemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        0=>array(
            'id'=>1,
            'name'=>'Certificate of Employement'
            'created_at'=>null,
            'updated_at'=>null
        ),
        1=>array(
            'id'=>2,
            'name'=>'HR Documents'
            'created_at'=>null,
            'updated_at'=>null
        ),
        2=>array(
            'id'=>3,
            'name'=>'Change equipment'
            'created_at'=>null,
            'updated_at'=>null
        ),
        3=>array(
            'id'=>4,
            'name'=>'Change workstations'
            'created_at'=>null,
            'updated_at'=>null
        )
        4=>array(
            'id'=>5,
            'name'=>'Request for equipment'
            'created_at'=>null,
            'updated_at'=>null
        ),
        5=>array(
            'id'=>6,
            'name'=>'Request/Update proximity access cards'
            'created_at'=>null,
            'updated_at'=>null
        ),
        6=>array(
            'id'=>7,
            'name'=>'Accounting services'
            'created_at'=>null,
            'updated_at'=>null
        ),
        7=>array(
            'id'=>8,
            'name'=>'On-the-Job Training'
            'created_at'=>null,
            'updated_at'=>null
        )
        8=>array(
            'id'=>9,
            'name'=>'Software/Hardware request'
            'created_at'=>null,
            'updated_at'=>null
        ),
        9=>array(
            'id'=>10,
            'name'=>'Network related concern'
            'created_at'=>null,
            'updated_at'=>null
        )
    }
}
