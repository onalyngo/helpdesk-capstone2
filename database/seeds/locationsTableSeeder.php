<?php

use Illuminate\Database\Seeder;

class locationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        0=>array(
            'id'=>1,
            'name'=>'Main: Makati'
            'created_at'=>null,
            'updated_at'=>null
        ),
        1=>array(
            'id'=>2,
            'name'=>'Branch: Alabang'
            'created_at'=>null,
            'updated_at'=>null
        ),
        2=>array(
            'id'=>3,
            'name'=>'Branch: Quezon City'
            'created_at'=>null,
            'updated_at'=>null
        ),
        3=>array(
            'id'=>4,
            'name'=>'Work From Home'
            'created_at'=>null,
            'updated_at'=>null
        )
    }
}
