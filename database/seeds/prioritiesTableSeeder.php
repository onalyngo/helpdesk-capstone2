<?php

use Illuminate\Database\Seeder;

class prioritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        0=>array(
            'id'=>1,
            'name'=>'Low'
            'created_at'=>null,
            'updated_at'=>null
        ),
        1=>array(
            'id'=>2,
            'name'=>'Normal'
            'created_at'=>null,
            'updated_at'=>null
        ),
        2=>array(
            'id'=>3,
            'name'=>'High'
            'created_at'=>null,
            'updated_at'=>null
        ),
        3=>array(
            'id'=>4,
            'name'=>'Urgent'
            'created_at'=>null,
            'updated_at'=>null
        )
    }
}
