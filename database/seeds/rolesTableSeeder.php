<?php

use Illuminate\Database\Seeder;

class rolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        0=>array(
            'id'=>1,
            'name'=>'admin'
            'created_at'=>null,
            'updated_at'=>null
        ),
        1=>array(
            'id'=>2,
            'name'=>'user'
            'created_at'=>null,
            'updated_at'=>null
        )
    }
}
